This project is a spring boot application
by default, it will start on port 8081

This project has two functions : 

to retrieve a list of superHero from Marvel (the list must be restricted using a limit and an offset)

example : GET <context>/heroes?offset=0&limit=20

to manage the user list of superHeroes
    - retrieve the list : GET <context>/MyHeroes
    - add hero to the list : PUT <context>/MyHeroes/{id} where id is the marvel id of the hero
    - remove hero from the list : DELETE <context>/MyHeroes/{id} where id is the marvel id of the hero


