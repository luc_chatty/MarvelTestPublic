package springboot;

import org.junit.jupiter.api.Test;
import springboot.model.HeroList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserControllerTest {
    private UserController controller = new UserController();

    @Test
    void returnHeroes() {
        this.addHero("1011334");
        HeroList heroList = this.retrieveHeroes();
        assertEquals(1, heroList.myHeroes.size());
    }

    /**
     * test add one hero
     */
    @Test
    void addHeroTest() {
        HeroList heroList = this.addHero("1011334");
        assertEquals(1, heroList.myHeroes.size());
        this.removeHero("1011334");
    }

    /**
     * test add one hero twice
     */
    @Test
    void addHeroTwiceTest() {
        this.addHero("1011334");
        HeroList heroList = this.addHero("1011334");
        assertEquals(1, heroList.myHeroes.size());
        this.removeHero("1011334");
    }

    /**
     * test add two hero
     */
    @Test
    void addTwoHeroTest()  {
        this.addHero("1017100");
        HeroList heroList = this.addHero("1011334");
        assertEquals(2, heroList.myHeroes.size());
        this.removeHero("1011334");
        this.removeHero("1017100");
    }

    /**
     * test remove one hero
     */
    @Test
    void removeHeroTest()  {
        this.addHero("1009144");
        HeroList heroList = this.removeHero("1009144");
        assertEquals(0, heroList.myHeroes.size());
        this.removeHero("1009144");
    }

    /**
     * add hero action
     * @param id the hero id
     * @return the heroList
     */
    private HeroList addHero(String id) {
        return controller.addHero(id);
    }

    /**
     * remove hero action
     * @param id the hero id
     * @return the heroList
     */
    private HeroList removeHero(String id) {
        return controller.deleteHero(id);
    }

    /**
     * retrieve heroes action
     * @return the heroList
     */
    private HeroList retrieveHeroes() {
        return controller.returnHeroes();
    }
}