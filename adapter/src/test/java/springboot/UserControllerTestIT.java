package springboot;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import springboot.model.HeroList;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserControllerTestIT {

    @Test
    void returnHeroes() {
        this.addHero("1011334");
        HeroList heroList = this.retrieveHeroes();
        assertEquals(1, heroList.myHeroes.size());
    }

    /**
     * test add one hero
     */
    @Test
    void addHeroTest() {
        HeroList heroList = this.addHero("1011334");
        assertEquals(1, heroList.myHeroes.size());
        this.removeHero("1011334");
    }

    /**
     * test add one hero twice
     */
    @Test
    void addHeroTwiceTest() {
        this.addHero("1011334");
        HeroList heroList = this.addHero("1011334");
        assertEquals(1, heroList.myHeroes.size());
        this.removeHero("1011334");
    }

    /**
     * test add two hero
     */
    @Test
    void addTwoHeroTest()  {
        this.addHero("1017100");
        HeroList heroList = this.addHero("1011334");
        assertEquals(2, heroList.myHeroes.size());
        this.removeHero("1011334");
        this.removeHero("1017100");
    }

    /**
     * test remove one hero
     */
    @Test
    void removeHeroTest()  {
        this.addHero("1009144");
        HeroList heroList = this.removeHero("1009144");
        assertEquals(0, heroList.myHeroes.size());
        this.removeHero("1009144");
    }

    /**
     * add hero action
     * @param id the hero id
     * @return the heroList
     */
    private HeroList addHero(String id) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8081/MyHeroes/" + id))
                    .PUT(HttpRequest.BodyPublishers.ofString(""))
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            String result = response.body();
            Gson gson = new Gson(); // Or use new GsonBuilder().create();
            return gson.fromJson(result, HeroList.class);
        } catch (InterruptedException | IOException | URISyntaxException e) {
            return null;
        }
    }

    /**
     * remove hero action
     * @param id the hero id
     * @return the heroList
     */
    private HeroList removeHero(String id) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8081/MyHeroes/" + id))
                    .DELETE()
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            String result = response.body();
            Gson gson = new Gson(); // Or use new GsonBuilder().create();
            return gson.fromJson(result, HeroList.class);
        } catch (InterruptedException | IOException | URISyntaxException e) {
            return null;
        }
    }

    /**
     * retrieve heroes action
     * @return the heroList
     */
    private HeroList retrieveHeroes() {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("http://localhost:8081/MyHeroes/" ))
                    .GET()
                    .build();
            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            String result = response.body();
            Gson gson = new Gson(); // Or use new GsonBuilder().create();
            return gson.fromJson(result, HeroList.class);
        } catch (InterruptedException | IOException | URISyntaxException e) {
            return null;
        }
    }
}