package springboot;

import com.google.gson.Gson;
import marvelApi.model.client.model.CharacterDataContainer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ControllerTestIT {

    @Test
    public void infoTest() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8081/info"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        String result = response.body();
        assertEquals("my marvel app", result);
    }

    @Test
    public void getHeroesTest() throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8081/heroes?offset=0&limit=20"))
                .GET()
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        String result = response.body();
        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        CharacterDataContainer characterDataContainer = gson.fromJson(result, CharacterDataContainer.class);
        assertEquals(20, characterDataContainer.getResults().size());
    }
}
