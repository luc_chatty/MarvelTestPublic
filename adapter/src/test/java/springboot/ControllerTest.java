package springboot;


import marvelApi.model.client.model.CharacterDataContainer;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ControllerTest {

    @Test
    void returnInfo() {
        Controller homeController = new Controller();
        String result = homeController.returnInfo();
        assertEquals(result, "my marvel app");

    }

    @Test
    public void getHeroesTest() throws URISyntaxException, IOException, InterruptedException {
        Controller homeController = new Controller();
        CharacterDataContainer characterDataContainer = homeController.returnHeroes(0,20);
        assertEquals(20, characterDataContainer.getResults().size());
    }
}