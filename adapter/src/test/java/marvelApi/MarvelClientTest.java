package marvelApi;

import marvelApi.model.client.ApiException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

class MarvelClientTest {

    @Test
    void getCharacterList() throws ApiException, InterruptedException, IOException, URISyntaxException, NoSuchAlgorithmException {
        MarvelClient client = new MarvelClient();
        assertEquals(20,client.getCharacterList(0, 20).getResults().size());
    }
}