package marvelApi;

import com.google.gson.Gson;
import marvelApi.model.client.api.DocspublicApi;
import marvelApi.model.client.model.Character;
import marvelApi.model.client.model.CharacterDataContainer;
import marvelApi.model.client.model.CharacterDataWrapper;
import security.HashGenerator;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Client to Marvel API
 */
public class MarvelClient extends DocspublicApi {


    /**
     * Get the list of Marvel characters
     * @return the list of desired characters
     * @param offset
     * @param limit
     */
    public CharacterDataContainer getCharacterList(int offset, int limit) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("https://gateway.marvel.com:443/v1/public/characters?ts=1"+"&limit="+limit+"&offset="+offset+"&apikey="+"2272e08231c8f2d8b8dff083981f5d91"+"&hash="+HashGenerator.getHash()))
                    .GET()
                    .build();

        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        return getCharacterDataContainerFromBody(response.body());
        } catch (InterruptedException | IOException | URISyntaxException e) {
            return null;
        }
    }

    public Character getCharacterById(String id) {
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("https://gateway.marvel.com:443/v1/public/characters/"+id+"?ts=1"+"&apikey="+"2272e08231c8f2d8b8dff083981f5d91"+"&hash="+HashGenerator.getHash()))
                    .GET()
                    .build();

            HttpResponse<String> response = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
            return getCharacterDataContainerFromBody(response.body()).getResults().get(0);
        } catch (InterruptedException | IOException | URISyntaxException e) {
            return null;
        }
    }

    private CharacterDataContainer getCharacterDataContainerFromBody(String body) {
        Gson gson = new Gson(); // Or use new GsonBuilder().create();
        return gson.fromJson(body, CharacterDataWrapper.class).getData();
    }

}
