package springboot;

import marvelApi.MarvelClient;
import marvelApi.model.client.model.CharacterDataContainer;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller{

    @RequestMapping("/info")
    /**
     * return app info
     */
    public String returnInfo() {
        return "my marvel app";
    }

    @CrossOrigin
    @RequestMapping(value = "/heroes")
    /**
     * return hero list
     */
    public CharacterDataContainer returnHeroes(@RequestParam int offset, @RequestParam int limit) {
        MarvelClient client = new MarvelClient();
        return client.getCharacterList(offset, limit);
    }

}
