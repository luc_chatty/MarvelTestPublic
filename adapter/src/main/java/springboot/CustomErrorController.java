package springboot;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomErrorController implements ErrorController {


    @RequestMapping("/error")
    public void defaultNotFound()
    {
        throw new NotFoundException();
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}
