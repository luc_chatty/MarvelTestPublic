package springboot;

import marvelApi.MarvelClient;
import marvelApi.model.client.model.Character;
import org.springframework.web.bind.annotation.*;
import springboot.model.HeroList;

import java.util.HashMap;
import java.util.stream.Collectors;

@RestController
public class UserController {
    public HashMap<String, Character> myHeroes = new HashMap<>();


    @CrossOrigin
    @RequestMapping(value = "/MyHeroes", method = RequestMethod.GET)
    /**
     * return my Heroes
     */
    public HeroList returnHeroes() {
        return returnAsList();
    }

    @CrossOrigin
    @RequestMapping(value = "/MyHeroes/{id}", method = RequestMethod.PUT)
    /**
     * add a hero
     */
    public HeroList addHero(@PathVariable String id) {
        MarvelClient client = new MarvelClient();
        myHeroes.put(id,client.getCharacterById(id));
        return returnAsList();
    }

    @CrossOrigin
    @RequestMapping(value = "/MyHeroes/{id}", method = RequestMethod.DELETE)
    /**
     * delete a hero
     */
    public HeroList deleteHero(@PathVariable String id) {
        MarvelClient client = new MarvelClient();
        myHeroes.remove(id);
        return returnAsList();
    }

    /**
     * convert map to list
     * @return list of hero member of the team
     */
    private HeroList returnAsList() {
        HeroList list = new HeroList();
        list.myHeroes = myHeroes.values().stream()
                .collect(Collectors.toList());
        return list;

    }
}
