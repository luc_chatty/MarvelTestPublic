package springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  Application {

    /**
     * spring boot application main class
     * @param args app args
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}